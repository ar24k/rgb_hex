export function convertToHex(r, g, b, outputElement) {
    if (r === '' || g === '' || b === '') {
        outputElement.textContent = 'Please fill in all fields.';
        return;
    }

    const hex = '#' + toHex(r) + toHex(g) + toHex(b);
    outputElement.textContent = `RGB: (${r}, ${g}, ${b}) to Hex: ${hex}`;
    const cardElement = document.querySelector('.card');
    cardElement.style.backgroundColor = hex;
}

export function convertToRGB(hexInput, outputElement) {
    if (!hexInput || hexInput.length !== 7 || !hexInput.startsWith('#')) {
        outputElement.textContent = 'Please enter a valid hex code (e.g., #ff0000).';
        return;
    }

    const r = parseInt(hexInput.slice(1, 3), 16);
    const g = parseInt(hexInput.slice(3, 5), 16);
    const b = parseInt(hexInput.slice(5, 7), 16);

    outputElement.textContent = `Hex: ${hexInput} to RGB: (${r}, ${g}, ${b})`;
    const cardElement = document.querySelector('.card');
    cardElement.style.backgroundColor = hexInput;
}

function toHex(n) {
    const hex = parseInt(n, 10).toString(16);
    return hex.length === 1 ? '0' + hex : hex;
}