import './style.css'
import javascriptLogo from './javascript.svg'
import viteLogo from '/vite.svg'
import { convertToHex, convertToRGB } from './converter.js'

document.addEventListener('DOMContentLoaded', function() {
  const convertToHexBtn = document.getElementById('convertToHexBtn');
  const convertToRGBBtn = document.getElementById('convertToRGBBtn');
  const redInput = document.getElementById('red');
  const greenInput = document.getElementById('green');
  const blueInput = document.getElementById('blue');
  const hexInput = document.getElementById('hex');
  const outputElement = document.getElementById('output');
  


  convertToHexBtn.addEventListener('click', () => {
      convertToHex(redInput.value, greenInput.value, blueInput.value, outputElement);
  });

  convertToRGBBtn.addEventListener('click', () => {
      convertToRGB(hexInput.value, outputElement);
  });
});



document.querySelector('#app').innerHTML = `
  <div>
    <a href="https://vitejs.dev" target="_blank">
      <img src="${viteLogo}" class="logo" alt="Vite logo" />
    </a>
    <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank">
      <img src="${javascriptLogo}" class="logo vanilla" alt="JavaScript logo" />
    </a>
    <h1>RGB/Hex Converter</h1>
    <div class="card">
      
      <div>
        <input type="number" id="red" placeholder="Red (0-255)" />
        <input type="number" id="green" placeholder="Green (0-255)" />
        <input type="number" id="blue" placeholder="Blue (0-255)" />
        <button id="convertToHexBtn">Convert to Hex</button>
      </div>
      <div>
        <input type="text" id="hex" placeholder="Enter Hex (e.g., #ff0000)" />
        <button id="convertToRGBBtn">Convert to RGB</button>
      </div>
      
      
    </div>
    <p id="output">RGB/Hex</p>
  </div>
`
